// import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_closesea_nft/render_collection_items.dart';
import 'package:google_fonts/google_fonts.dart';
// import 'package:nft_museum/apis/raribel.dart';
// import 'package:nft_museum/pages/owner.dart';
// import 'package:nft_museum/utils/default_loading.dart';
// import 'package:nft_museum/widgets/render_collection_items.dart';
import 'package:readmore/readmore.dart';

int renderUpto = 8; // default render items

class ShowCollection extends StatefulWidget {
  // final String address;
  const ShowCollection({
    Key? key,
  }) : super(key: key);

  @override
  State<ShowCollection> createState() => _ShowCollectionState();
}

class _ShowCollectionState extends State<ShowCollection> {
  var staticData = [
    {
      'properties': 'Bayon Temple',
      'ownership': '0.4',
      'image': 'assets/art/444.jpeg'
    },
    {
      'properties': 'Statue',
      'ownership': '0.4',
      'image': 'assets/art/statue.jpeg'
    },
    {
      'properties': 'Statue',
      'ownership': '0.4',
      'image': 'assets/art/statue1.jpeg'
    },
    {
      'properties': 'Statue',
      'ownership': '0.4',
      'image': 'assets/art/statue2.jpeg'
    },
    {'properties': '', 'ownership': '0.4', 'image': 'assets/art/444.jpeg'},
  ];
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        body:
            // FutureBuilder(
            //   future: RaribleAPI().collectionByRawAddress(widget.address),
            //   builder: (context, snapshot) {
            //     switch (snapshot.connectionState) {
            //       case (ConnectionState.waiting):
            //         return Center(child: defaultLoading());
            //       default:
            //         {
            //           if (snapshot.hasError) {
            //             return Center(child: defaultError());
            //           } else {
            //             return
            SingleChildScrollView(
          child: collectionBody(
              // collectionData: snapshot.data, address: widget.address),
              ),
          //           }
          //         }
          //     }
          //   },
        ),
      ),
    );
  }

  Column collectionBody() {
    String posterImageUrl =
        'https://i.pinimg.com/564x/b4/b9/f3/b4b9f3ec6d313d19bb43aca250682bfd.jpg';
    // collectionData['cover'].startsWith("ipfs://")
    //     ? collectionData['cover'].replaceAll('ipfs://', 'https://ipfs.io/')
    //     : collectionData['cover'];
    String picUrl =
        'https://i.pinimg.com/564x/b4/b9/f3/b4b9f3ec6d313d19bb43aca250682bfd.jpg';
    // collectionData['pic'].startsWith("ipfs://")
    //     ? collectionData['pic'].replaceAll('ipfs://', 'https://ipfs.io/')
    //     : collectionData['pic'];
    String name = "Cambodian art";
    String description =
        "Cambodia's best-known stone carving adorns the temples of Angkor, which are renowned for the scale, richness and detail of their sculpture. In modern times, however, the art of stone carving became rare, largely because older sculptures survived undamaged for centuries (eliminating the need for replacements) and because of the use of cement molds for modern temple architecture. ";
    String owner = "Wing NFT";
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SizedBox(
          height: 220,
          child: Stack(
            children: <Widget>[
              SizedBox(
                width: MediaQuery.of(context).size.width,
                height: 200,
                child: Image.asset(
                  'assets/art/555.jpg',
                  fit: BoxFit.cover,
                ),
                // CachedNetworkImage(
                //   imageUrl: posterImageUrl,
                //   placeholder: (context, str) =>
                //       // Center(child: defaultLoading()),
                //       const Center(
                //     child: CircularProgressIndicator(),
                //   ),
                //   errorWidget: (context, str, dyn) =>
                //       const Center(child: Text("Something Went Wrong")),
                //   fit: BoxFit.cover,
                // ),
              ),
              Positioned(
                width: 100,
                height: 100,
                top: 120,
                left: 20,
                child: Container(
                  width: 100,
                  height: 100,
                  decoration: BoxDecoration(
                    color: Colors.white, //const Color.fromRGBO(25, 28, 31, 1),
                    borderRadius: BorderRadius.circular(20),
                    border: Border.all(
                      width: 4,
                      color:
                          Colors.white, //const Color.fromRGBO(25, 28, 31, 1),
                    ),
                  ),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(20),
                    child: Image.asset(
                      'assets/art/111.jpeg',
                      fit: BoxFit.cover,
                    ),
                    // CachedNetworkImage(
                    //   imageUrl: picUrl,
                    //   placeholder: (context, str) => const Center(
                    //     child: CircularProgressIndicator(),
                    //   ),
                    //   errorWidget: (context, str, dyn) =>
                    //       const Center(child: Text("Something Went Wrong")),
                    //   fit: BoxFit.cover,
                    // ),
                  ),
                ),
              ),
              Positioned(
                child: IconButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  icon: Icon(
                    Icons.arrow_back_ios,
                    color: Colors.white,
                  ),
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 10, left: 20),
          child: Text(
            name,
            style: GoogleFonts.inder(
              fontSize: 28,
              fontWeight: FontWeight.normal,
              color: Colors.black,
            ),
            overflow: TextOverflow.ellipsis,
          ),
        ),
        owner.isNotEmpty
            ? Padding(
                padding: const EdgeInsets.only(top: 10, left: 20),
                child: InkWell(
                  onTap: () {
                    // Navigator.push(
                    //   context,
                    //   MaterialPageRoute(
                    //     builder: (context) => Owner(walletAddress: owner),
                    //   ),
                    // );
                  },
                  child: RichText(
                    overflow: TextOverflow.ellipsis,
                    text: TextSpan(
                      text: "Created by ",
                      style: GoogleFonts.inter(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                        color: const Color.fromRGBO(140, 140, 140, 1),
                      ),
                      children: <TextSpan>[
                        TextSpan(
                          text: owner,
                          // "${owner.substring(0, 6)}...${owner.substring(owner.length - 4, owner.length - 1)}",
                          style: GoogleFonts.inter(
                            fontSize: 14,
                            fontWeight: FontWeight.w600,
                            color: Colors.black,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              )
            : const SizedBox(),
        Padding(
          padding: const EdgeInsets.only(top: 10, left: 20, right: 10),
          child: ReadMoreText(
            description,
            colorClickableText: Colors.black,
            trimMode: TrimMode.Line,
            trimCollapsedText: 'Show More ▼',
            trimExpandedText: '\nShow Less ▲',
            style: GoogleFonts.inter(
              fontSize: 15,
              color: const Color.fromRGBO(140, 140, 140, 1),
              fontWeight: FontWeight.normal,
            ),
          ),
        ),
        const SizedBox(
          height: 30,
        ),
        Center(
          child: Container(
            padding: const EdgeInsets.all(20),
            height: 180,
            width: MediaQuery.of(context).size.width * 0.8,
            decoration: BoxDecoration(
              border: Border.all(
                color: Color.fromARGB(24, 70, 69, 69),
                width: 1,
              ),
              borderRadius: BorderRadius.circular(20),
            ),
            child: ListView(
              physics: const NeverScrollableScrollPhysics(),
              children: <Widget>[
                listTile(leading: "Floor", trailing: '6.7ETH'),
                listTile(leading: "Volume", trailing: '8.9ETH'),
                listTile(leading: "Items", trailing: '2.0k'),
                listTile(leading: "Owners", trailing: '400'),
              ],
            ),
            //  FutureBuilder(
            //   future: RaribleAPI().collectionStats(widget.address),
            //   builder: (context, snapshot) {
            //     switch (snapshot.connectionState) {
            //       case (ConnectionState.waiting):
            //         return  const Center(
            //             child: CircularProgressIndicator(),
            //           );
            //       default:
            //         {

            //           if (snapshot.hasError) {
            //             return Center(
            //               child: defaultError(),
            //             );
            //           } else {

            //             dynamic data = snapshot.data;

            //             String floor, volume, items, owners, currency;
            //             items = modified(data['items']);
            //             owners = modified(data['owners']);
            //             if (data.containsKey('nativeVolume')) {
            //               floor = modified(data['nativeVolume']['floorPrice']);
            //               volume = modified(data['nativeVolume']['volume']);
            //               currency = data['nativeVolume']['currency'];
            //             } else {
            //               floor = modified(data['usdVolume']['floorPrice']);
            //               volume = modified(data['usdVolume']['volume']);
            //               currency = data['usdVolume']['currency'];
            //             }
            //             return ListView(
            //               physics: const NeverScrollableScrollPhysics(),
            //               children: <Widget>[
            //                 listTile(
            //                     leading: "Floor", trailing: floor + currency),
            //                 listTile(
            //                     leading: "Volume", trailing: volume + currency),
            //                 listTile(leading: "Items", trailing: items),
            //                 listTile(leading: "Owners", trailing: owners),
            //               ],
            //             );
            //           }

            //         }
            //     }
            //   },
            // ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(10.0),
          child: SizedBox(
            width: MediaQuery.of(context).size.width,
            child: Text(
              "Items",
              style: GoogleFonts.inter(fontSize: 24),
              textAlign: TextAlign.center,
            ),
          ),
        ),
        RenderCollectionItems(
          staticData: staticData,
          // address: address,
          // renderUpto: renderUpto,
        ),
        const SizedBox(height: 20),
        renderUpto == 8
            ? InkWell(
                onTap: () {
                  // TODO : TO MAKE A SEPARATE SCREEN FOR IT
                  setState(() {
                    renderUpto = 100;
                  });
                },
                child: Center(
                  child: Container(
                    width: 190,
                    height: 48,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(15),
                    ),
                    child: Center(
                      child: Text(
                        "See More Items",
                        style: GoogleFonts.inter(
                          fontSize: 14,
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                ),
              )
            : const SizedBox(),
      ],
    );
  }
}

Widget listTile({required String leading, required String trailing}) {
  return Padding(
    padding: const EdgeInsets.all(8.0),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          leading,
          style: GoogleFonts.inter(
            fontSize: 15,
            fontWeight: FontWeight.w600,
            color: Colors.black,
          ),
        ),
        Text(
          trailing,
          style: GoogleFonts.inter(
            fontSize: 14,
            color: Colors.black,
          ),
        ),
      ],
    ),
  );
}

String modified(num value) {
  if (value >= 1000000) {
    return "${(value / 1000000).toStringAsFixed(1)}M";
  } else if (value >= 1000) {
    return "${(value / 1000).toStringAsFixed(1)}K";
  }
  return value.toString();
}
