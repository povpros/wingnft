import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
// import 'package:nft_museum/apis/raribel.dart';
// import 'package:nft_museum/utils/default_loading.dart';

class RenderCollectionItems extends StatefulWidget {
  final List staticData;
  // final int renderUpto;
  const RenderCollectionItems({
    Key? key,
    required this.staticData,
  }) : super(key: key);

  @override
  State<RenderCollectionItems> createState() => _RenderCollectionItemsState();
}

class _RenderCollectionItemsState extends State<RenderCollectionItems> {
  List<dynamic> rawData = [];
  List<Map<String, dynamic>> data = [];
  @override
  void initState() {
    super.initState();
    collectData();
  }

  // var staticData = [
  //   {
  //     'properties': 'Bayon Temple',
  //     'ownership': 'Wing NFT',
  //     'image': 'assets/art/444.jpeg'
  //   },
  //   {
  //     'properties': 'Statue',
  //     'ownership': 'Wing NFT',
  //     'image': 'assets/art/statue.jpeg'
  //   },
  //   {
  //     'properties': 'Statue',
  //     'ownership': 'Wing NFT',
  //     'image': 'assets/art/statue1.jpeg'
  //   },
  //   {
  //     'properties': 'Statue',
  //     'ownership': 'Wing NFT',
  //     'image': 'assets/art/statue2.jpeg'
  //   },
  //   {'properties': '', 'ownership': 'Wing NFT', 'image': 'assets/art/444.jpeg'},
  // ];

  void collectData() async {
    // rawData = await RaribleAPI().getDataOfItemsOfCollection(widget.address, widget.renderUpto);
    for (var d in widget.staticData) {
      Map<String, dynamic> temp = {};
      temp["name"] = d["properties"] ?? "Unknown";
      temp["price"] = d["ownership"] ?? "Unknown";
      temp["image"] = d["image"] ?? "Unknown";
      data.add(temp);
    }
    // print(data);
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return data.isEmpty
        ? const SizedBox(
            height: 500,
            width: double.infinity,
            child: Center(
              child: CircularProgressIndicator(),
            ),
          )
        : Center(
            child: GridView.builder(
              shrinkWrap: true,
              padding: const EdgeInsets.symmetric(horizontal: 10),
              physics: const NeverScrollableScrollPhysics(),
              itemCount: data.length,
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                childAspectRatio: 1.0,
                crossAxisSpacing: 0.0,
                mainAxisSpacing: 5,
                mainAxisExtent: 250,
              ),
              itemBuilder: (context, index) {
                final item = data[index];
                return Container(
                  //   height: 400,
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Color.fromARGB(24, 70, 69, 69),
                      width: 2,
                    ),
                    borderRadius: BorderRadius.circular(20),
                  ),
                  margin: const EdgeInsets.all(5),
                  padding: const EdgeInsets.all(10),
                  child: Stack(
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Expanded(
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(10),
                              child: Image.asset(
                                item["image"],
                                fit: BoxFit.cover,
                              ),
                              // CachedNetworkImage(
                              //   imageUrl: item["image"],
                              //   fit: BoxFit.cover,
                              //   errorWidget: (context, str, d) =>
                              //       defaultError(),
                              // ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(4.0),
                            child: Text(
                              item['name'],
                              style: GoogleFonts.inter(
                                fontSize: 16,
                                color: Colors.black,
                                fontWeight: FontWeight.w500,
                              ),
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(2),
                            child: Row(
                              children: [
                                Text(
                                  "Price: ${item['price']}",
                                  style: GoogleFonts.inter(
                                    fontSize: 11,
                                    color: Colors.black,
                                  ),
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                );
              },
            ),
          );
  }
}
